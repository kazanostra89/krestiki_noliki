﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameMetods
{
    class Program
    {
        const int MOVE_PLAYER_ONE = 1, MOVE_PLAYER_TWO = 2, X_LIKE = 8,
                  O_LIKE = 55, ZERO_VALUE_ARRAY = 0, GAME_DRAW = 9; 

        static void ZapolnMassZero(int[,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    mas[i, j] = ZERO_VALUE_ARRAY;
                }
            }
        }

        static void PrintMass(int[,] mas)
        {
            Console.Clear();

            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    switch (mas[i, j])
                    {
                        case ZERO_VALUE_ARRAY:
                            Console.Write(".");
                            break;
                        case X_LIKE:
                            Console.Write("X");
                            break;
                        case O_LIKE:
                            Console.Write("O");
                            break;
                    }
                }
                Console.WriteLine();
            }
        }

        static void StepPlayer(int[,] mas, string message, int nX)
        {
            int coordI, coordJ;
            bool checkI, checkJ;
            Console.WriteLine(message);

            do
            {
                Console.Write("Введите coordI: ");
                checkI = int.TryParse(Console.ReadLine(), out coordI);

                Console.Write("Введите coordJ: ");
                checkJ = int.TryParse(Console.ReadLine(), out coordJ);

            } while (checkI == false || checkJ == false || coordI < 0 || coordI > 2
                    || coordJ < 0 || coordJ > 2 || mas[coordI, coordJ] == X_LIKE
                    || mas[coordI, coordJ] == O_LIKE);

            if (mas[coordI, coordJ] == ZERO_VALUE_ARRAY)
            {
                mas[coordI, coordJ] = nX;
            }

        }

        static void CheckPlayerWinHorDiag(int[,] mas, string message, int nX, ref bool playGame)
        {
            int summaHoriz, summaDiagLR, summaDiagRL;
            summaHoriz = summaDiagLR = summaDiagRL = 0;

            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {

                    if (mas[i, j] == nX)
                    {
                        summaHoriz += nX;

                        if (summaHoriz == (nX * 3))
                        {
                            PrintMass(mas);
                            Console.WriteLine(message + " Horizontal");
                            playGame = false;
                        }
                    }

                    if ((i == j && mas[i, j] == nX) || ((j == mas.GetLength(1) - 1 - i) && mas[i, j] == nX))
                    {
                        if (i == j)
                        {
                            summaDiagLR += nX;
                        }
                        else
                        {
                            summaDiagRL += nX;
                        }

                        if (summaDiagLR == (nX * 3) || summaDiagRL == (nX * 3))
                        {
                            PrintMass(mas);
                            Console.WriteLine(message + " Diagonal");
                            playGame = false;
                        }
                    }
                }

                summaHoriz = 0;
            }
        }

        static void CheckPlayerWinVertic(int[,] mas, string message, int nX, ref bool playGame)
        {
            int summaVert = 0;

            for (int j = 0; j < mas.GetLength(1); j++)
            {
                for (int i = 0; i < mas.GetLength(0); i++)
                {
                    if (mas[i, j] == nX)
                    {
                        summaVert += nX;

                        if (summaVert == (nX * 3))
                        {
                            PrintMass(mas);
                            Console.WriteLine(message + " Vertical");
                            playGame = false;
                        }
                    }
                }

                summaVert = 0;
            }
        }

        static void CheckPlayersEquality(ref int summaSteps, ref bool playGame, int[,] mas)
        {
            summaSteps++;

            if (summaSteps == GAME_DRAW && playGame)
            {
                playGame = false;
                PrintMass(mas);
                Console.WriteLine("Ничья!!!");
            }
        }

        static void Main(string[] args)
        {
            int sizeMas, currentStep, summaSteps;
            
            bool playGame;
           
            sizeMas = 3;
            currentStep = MOVE_PLAYER_ONE;
            summaSteps = 0;
            playGame = true;
                        
            int[,] mas = new int[sizeMas, sizeMas];
                        
            ZapolnMassZero(mas);

            while (playGame)
            {
                PrintMass(mas);

                switch (currentStep)
                {
                    case MOVE_PLAYER_ONE:
                        StepPlayer(mas, "Ходит игрок №1: ", X_LIKE);
                        CheckPlayerWinHorDiag(mas, "Win player №1", X_LIKE, ref playGame);
                        CheckPlayerWinVertic(mas, "Win player №1", X_LIKE, ref playGame);
                        currentStep = MOVE_PLAYER_TWO;
                        break;
                    case MOVE_PLAYER_TWO:
                        StepPlayer(mas, "Ходит игрок №2: ", O_LIKE);
                        CheckPlayerWinHorDiag(mas, "Win player №2", O_LIKE, ref playGame);
                        CheckPlayerWinVertic(mas, "Win player №2", O_LIKE, ref playGame);
                        currentStep = MOVE_PLAYER_ONE;
                        break;
                }

                CheckPlayersEquality(ref summaSteps, ref playGame, mas);
            }

            Console.ReadKey();
        }
    }
}


