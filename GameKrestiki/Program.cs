﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameKrestiki
{
    class Program
    {
        static void Main(string[] args)
        {
            int coordI, coordJ, currentStep, summaHoriz, summaDiagRL, summaDiagLR, summaVert;
            int sizeMas;
            bool checkI, checkJ, playGame;

            int summa2Horiz, summa2DiagRL, summa2DiagLR, summa2Vert, summaSteps = 0;

            sizeMas = 3;
            currentStep = 1;
            playGame = true;

            int[,] mas = new int[sizeMas, sizeMas];

            for (int i = 0; i < sizeMas; i++)
            {
                for (int j = 0; j < sizeMas; j++)
                {
                    mas[i, j] = 0;
                }
            }

            #region Цикл игры            
            while (playGame)
            {

                #region Вывод игрового поля
                Console.Clear();

                for (int i = 0; i < sizeMas; i++)
                {
                    for (int j = 0; j < sizeMas; j++)
                    {
                        switch (mas[i, j])
                        {
                            case 0:
                                Console.Write(".");
                                break;
                            case 8:
                                Console.Write("X");
                                break;
                            case 55:
                                Console.Write("O");
                                break;
                        }
                    }
                    Console.WriteLine();
                }
                #endregion

                #region Смена ходов пользователей
                switch (currentStep)
                {
                    case 1:
                        Console.WriteLine("Ходит игрок №1: ");

                        do
                        {
                            Console.Write("Введите coordI: ");
                            checkI = int.TryParse(Console.ReadLine(), out coordI);

                            Console.Write("Введите coordJ: ");
                            checkJ = int.TryParse(Console.ReadLine(), out coordJ);

                        } while (checkI == false || checkJ == false || coordI < 0 || coordI > 2
                                || coordJ < 0 || coordJ > 2 || mas[coordI, coordJ] == 8
                                || mas[coordI, coordJ] == 55);

                        if (mas[coordI, coordJ] == 0)
                        {
                            mas[coordI, coordJ] = 8;
                        }

                        currentStep = 2;
                        break;
                    case 2:
                        Console.WriteLine("Ходит игрок №2: ");

                        do
                        {
                            Console.Write("Введите coordI: ");
                            checkI = int.TryParse(Console.ReadLine(), out coordI);

                            Console.Write("Введите coordJ: ");
                            checkJ = int.TryParse(Console.ReadLine(), out coordJ);

                        } while (checkI == false || checkJ == false || coordI < 0 || coordI > 2
                                || coordJ < 0 || coordJ > 2 || mas[coordI, coordJ] == 8
                                || mas[coordI, coordJ] == 55);

                        if (mas[coordI, coordJ] == 0)
                        {
                            mas[coordI, coordJ] = 55;
                        }

                        currentStep = 1;
                        break;
                }
                #endregion

                #region Проверка первого игрока
                summaHoriz = summaDiagRL = summaDiagLR = summaVert = 0;

                for (int i = 0; i < sizeMas; i++)
                {
                    for (int j = 0; j < sizeMas; j++)
                    {

                        if (mas[i, j] == 8)
                        {
                            summaHoriz+=8;

                            if (summaHoriz == 24)
                            {
                                Console.WriteLine("Win User №1 Horizontal");

                                playGame = false;
                            }

                        }

                        if ((i == j && mas[i, j] == 8) || ((j == sizeMas - 1 - i) && mas[i, j] == 8))
                        {
                            if (i == j)
                            {
                                summaDiagLR += 8;
                            }
                            else
                            {
                                summaDiagRL += 8;
                            }

                            if (summaDiagLR == 24 || summaDiagRL == 24)
                            {
                                Console.WriteLine("Win User №1 Diagonal");

                                playGame = false;
                            }
                        }

                    }
                    summaHoriz = 0;
                }

                for (int j = 0; j < sizeMas; j++)
                {
                    for (int i = 0; i < sizeMas; i++)
                    {
                        if (mas[i, j] == 8)
                        {
                            summaVert+=8;

                            if (summaVert == 24)
                            {
                                Console.WriteLine("Win User №1 Vertical");

                                playGame = false;
                            }

                        }
                    }

                    summaVert = 0;
                }
                #endregion

                #region Проверка второго игрока
                
                summa2Horiz = summa2DiagRL = summa2DiagLR = summa2Vert = 0;

                for (int i = 0; i < sizeMas; i++)
                {
                    for (int j = 0; j < sizeMas; j++)
                    {

                        if (mas[i, j] == 55)
                        {
                            summa2Horiz+= 55;

                            if (summa2Horiz == 165)
                            {
                                Console.WriteLine("Win User №2 Horizontal");
                                Console.ReadKey();
                                playGame = false;
                            }

                        }

                        if ((i == j && mas[i, j] == 55) || ((j == sizeMas - 1 - i) && mas[i, j] == 55))
                        {
                            if (i == j)
                            {
                                summa2DiagLR += 55;
                            }
                            else
                            {
                                summa2DiagRL += 55;
                            }

                            if (summa2DiagLR == 165 || summa2DiagRL == 165)
                            {
                                Console.WriteLine("Win User №2 Diagonal");
                                Console.ReadKey();
                                playGame = false;
                            }
                        }

                    }
                    summa2Horiz = 0;
                }

                for (int j = 0; j < sizeMas; j++)
                {
                    for (int i = 0; i < sizeMas; i++)
                    {
                        if (mas[i, j] == 55)
                        {
                            summa2Vert+= 55;

                            if (summa2Vert == 165)
                            {
                                Console.WriteLine("Win User №2 Vertical");
                                Console.ReadKey();
                                playGame = false;
                            }

                        }
                    }

                    summa2Vert = 0;
                }

                #endregion
                summaSteps++;

                if (summaSteps == 9 && playGame)
                {
                    playGame = false;
                    Console.WriteLine("Ничья!!!");
                }

            }
            #endregion



            Console.ReadKey();
        }


    }
}
